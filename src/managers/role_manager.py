from pony.orm import commit, select

from helpers.helpers import Helper


class RoleManager:
    def __init__(self, database):
        self.db = database

    def create_role(self, is_admin):
        params = {
            "is_admin": is_admin,
            "token": Helper.generate_random_string(64)
        }

        contact = self.db.Role(**params)
        commit()
        return contact

    def get_role_by_token(self, token):
        params = {
            "token": token
        }

        role = self.db.Role.get(**params)
        return role

    def delete_role_by_token(self, token):
        params = {
            "token": token
        }

        role = self.db.Role.get(**params)
        if role is None:
            return True
        role.delete()
        return True
    
