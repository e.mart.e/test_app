from pony.orm import commit, select


class ContactsManager:
    def __init__(self, database):
        self.db = database

    def create_contact(self, creator_id, name, last_name, patronymic, job_title, email, phone):
        params = {
            "creator_id": creator_id,
            "name": name,
            "last_name": last_name,
            "patronymic": patronymic,
            "job_title": job_title,
            "email": email,
            "phone": phone
        }

        contact = self.db.Contact(**params)
        commit()
        return contact

    def update_admin_contact(self, contact_id, new_params):
        get_params = {
            "id": contact_id
        }

        contact = self.db.Contact.get(**get_params)

        if contact is None:
            print("Contact not found")
            return None

        params = ("creator_id", "name", "last_name", "patronymic", "job_title", "email", "phone")

        for param in params:
            try:
                new_param = new_params[param]
                if param == "creator_id":
                    contact.creator_id = new_param
                if param == "name":
                    contact.name = new_param
                if param == "last_name":
                    contact.last_name = new_param
                if param == "patronymic":
                    contact.patronymic = new_param
                if param == "job_title":
                    contact.job_title = new_param
                if param == "email":
                    contact.email = new_param
                if param == "phone":
                    phone = str(new_param)
                    if 10 <= len(phone) <= 12:
                        if phone[0] == '8' and len(phone) == 11:
                            phone = '+7' + phone[1:]
                        elif phone[0] == '9' and len(phone) == 10:
                            phone = '+7' + phone
                        elif phone[0] == '7' and len(phone) == 11:
                            phone = '+' + phone
                        elif phone[0] == '+' and len(phone) == 12:
                            phone = phone
                    contact.phone = phone
            except:
                pass

        commit()
        return contact

    def update_contact(self, contact_id, creator_id, new_params):
        get_params = {
            "id": contact_id,
            "creator_id": creator_id,
            "is_deleted": False
        }

        contact = self.db.Contact.get(**get_params)

        if contact is None:
            print("Contact not found")
            return None

        params = ("name", "last_name", "patronymic", "job_title", "email", "phone")

        for param in params:
            try:
                new_param = new_params[param]
                if param == "name":
                    if new_param is None:
                        continue
                    contact.name = new_param
                if param == "last_name":
                    if new_param is None:
                        continue
                    contact.last_name = new_param
                if param == "patronymic":
                    contact.patronymic = new_param
                if param == "job_title":
                    if new_param is None:
                        continue
                    contact.job_title = new_param
                if param == "email":
                    if new_param is None:
                        continue
                    contact.email = new_param
                if param == "phone":
                    if new_param is None:
                        continue
                    phone = str(new_param)
                    if 10 <= len(phone) <= 12:
                        if phone[0] == '8' and len(phone) == 11:
                            phone = '+7' + phone[1:]
                        elif phone[0] == '9' and len(phone) == 10:
                            phone = '+7' + phone
                        elif phone[0] == '7' and len(phone) == 11:
                            phone = '+' + phone
                        elif phone[0] == '+' and len(phone) == 12:
                            phone = phone
                    contact.phone = phone
            except:
                pass

        commit()
        return contact

    def delete_admin_contact(self, contact_id):
        id = {
            "id": contact_id
        }

        contact = self.db.Contact.get(**id)
        contact.is_deleted = True
        commit()

        return contact

    def delete_contact(self, contact_id, creator_id):
        id = {
            "id": contact_id,
            "creator_id": creator_id
        }

        contact = self.db.Contact.get(**id)
        if contact is None:
            return None
        contact.is_deleted = True
        commit()

        return contact

    def filter_out_contacts(self, param):
        if len(param) != 1:
            print("invalid parameters passed")
            return

        key = None
        for key in param.keys():
            key = key

        if key is None:
            print("invalid parameters passed")
            return

        query = select(c
                       for c in self.db.Contact
                       if c.is_deleted is False)

        if key == "name":
            query = select(c
                           for c in self.db.Contact
                           if c.name == param.get(key)
                           and c.is_deleted is False)
        if key == "last_name":
            query = select(c
                           for c in self.db.Contact
                           if c.last_name == param.get(key)
                           and c.is_deleted is False)
        if key == "patronymic":
            query = select(c
                           for c in self.db.Contact
                           if c.patronymic == param.get(key)
                           and c.is_deleted is False)
        if key == "job_title":
            query = select(c
                           for c in self.db.Contact
                           if c.job_title == param.get(key)
                           and c.is_deleted is False)
        if key == "email":
            query = select(c
                           for c in self.db.Contact
                           if c.email == param.get(key)
                           and c.is_deleted is False)
        if key == "phone":
            query = select(c
                           for c in self.db.Contact
                           if c.phone == param.get(key)
                           and c.is_deleted is False)
        if key == "creator_id":
            query = select(c
                           for c in self.db.Contact
                           if c.creator_id == param.get(key)
                           and c.is_deleted is False)

        return query

    def sort_contacts(self, contact_atr):
        query = select(c for c in self.db.Contact
                       if c.is_deleted is False)

        my_list = []
        for contact in query:
            contact = contact.to_web_dict()
            my_list.append(contact)

        my_list.sort(key=lambda dictionary: dictionary[contact_atr])

        return my_list


    def get_contact(self, contact_id):
        id = {
            "id": contact_id,
            "is_deleted": False
        }

        contact = self.db.Contact.get(**id)
        return contact

    def get_admin_contact(self, contact_id):
        id = {
            "id": contact_id,
        }

        contact = self.db.Contact.get(**id)
        return contact
    
    def get_admin_contacts(self):
        query = select(c for c in self.db.Contact)
        
        return query

    def get_contacts(self):
        query = select(c for c in self.db.Contact
                       if c.is_deleted is False)

        return query


