from config import APP_SETTINGS as S


def api_path(prefix, method_name):
    return '%s/%s' % (prefix, method_name)


def is_web_token_correct(token):
    return token in S['default_token']
