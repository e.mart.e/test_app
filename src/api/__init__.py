import logging

from .blueprints.web import contact_api
from .blueprints.web import role_api

logger = logging.getLogger(__name__)


def register_blueprints(app, config):
    api_prefix = config['api']['prefix']
    api_version = 'v%s' % config['api']['version']

    app.register_blueprint(contact_api.construct_blueprint(api_prefix))
    app.register_blueprint(role_api.construct_blueprint(api_prefix))

