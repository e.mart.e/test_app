import logging

from flask import Blueprint, request
from pony.orm import db_session

import config
from constants import ApiFrontEndResponseStatus
from api.api_manager import ApiManager
from api.utils.common import api_path
from db import database
from managers.contacts_manager import ContactsManager
from managers.role_manager import RoleManager

logger = logging.getLogger(__name__)


class ApiMethods:
    createContact = 'createContact'
    updateAdminContact = 'updateAdminContact'
    updateContact = 'updateContact'
    deleteAdminContact = 'deleteAdminContact'
    deleteContact = 'deleteContact'
    filterOutContacts = 'filterOutContacts'
    sortContacts = 'sortContacts'
    getContact = 'getContact'
    getContacts = 'getContacts'
    getAdminContact = 'getAdminContact'
    getAdminContacts = 'getAdminContacts'


role_manager = RoleManager(database)
contact_manager = ContactsManager(database)


def construct_blueprint(api_prefix):
    bp = Blueprint('contacts_api', __name__)

    @bp.route(api_path(api_prefix, ApiMethods.getAdminContacts), methods=['POST'])
    @db_session
    def handle_get_admin_contacts():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        if role.is_admin is False:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Not admin')

        contacts = contact_manager.get_admin_contacts()
        list_contacts = []

        for contact in contacts:
            contact = contact.to_web_dict()
            list_contacts.append(contact)

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, list_contacts)

    @bp.route(api_path(api_prefix, ApiMethods.getAdminContact), methods=['POST'])
    @db_session
    def handle_get_admin_contact():
        body = request.get_json()

        user_token = body.get('token')
        contact_id = body.get('contact_id')

        if contact_id is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact id not found')

        if user_token is None:
            return ApiManager.return_unauthorized()

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        if role.is_admin is False:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Not admin')

        contact = contact_manager.get_admin_contact(contact_id)
        if contact is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact not found')

        result = contact.to_web_dict()

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    @bp.route(api_path(api_prefix, ApiMethods.getContacts), methods=['POST'])
    @db_session
    def handle_get_contacts():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        contacts = contact_manager.get_contacts()
        list_contacts = []

        for contact in contacts:
            contact = contact.to_web_dict()
            list_contacts.append(contact)

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, list_contacts)

    @bp.route(api_path(api_prefix, ApiMethods.getContact), methods=['POST'])
    @db_session
    def handle_get_contact():
        body = request.get_json()

        user_token = body.get('token')
        contact_id = body.get('contact_id')

        if contact_id is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact id not found')

        if user_token is None:
            return ApiManager.return_unauthorized()

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        contact = contact_manager.get_contact(contact_id)
        if contact is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact not found')

        result = contact.to_web_dict()

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    @bp.route(api_path(api_prefix, ApiMethods.sortContacts), methods=['POST'])
    @db_session
    def handle_sort_contacts():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        attr_name = body.get('attr_name')

        if attr_name is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Not found attr name')

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        contacts = contact_manager.sort_contacts(attr_name)
        list_contacts = []

        for contact in contacts:
            list_contacts.append(contact)

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, list_contacts)

    @bp.route(api_path(api_prefix, ApiMethods.filterOutContacts), methods=['POST'])
    @db_session
    def handle_filter_out_contacts():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        attr = body.get('attr')

        if attr is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Not found attr')

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        contacts = contact_manager.filter_out_contacts(attr)
        list_contacts = []

        for contact in contacts:
            contact = contact.to_web_dict()
            list_contacts.append(contact)

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, list_contacts)

    @bp.route(api_path(api_prefix, ApiMethods.deleteAdminContact), methods=['POST'])
    @db_session
    def handle_delete_admin_contact():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        contact_id = body.get('contact_id')

        if contact_id is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact id not found')

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        if role.is_admin is False:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Not admin')

        contact = contact_manager.delete_admin_contact(contact_id)
        if contact is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact not found')

        result = contact.to_web_dict()

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    @bp.route(api_path(api_prefix, ApiMethods.deleteContact), methods=['POST'])
    @db_session
    def handle_delete_contact():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        contact_id = body.get('contact_id')

        if contact_id is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact id not found')

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        if role.is_admin is True:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'This admin use any method')

        contact = contact_manager.delete_contact(contact_id, role.id)
        if contact is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact not found')

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, 'contact successfully deleted')

    @bp.route(api_path(api_prefix, ApiMethods.updateContact), methods=['POST'])
    @db_session
    def handle_update_contact():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        contact_id = body.get('contact_id')

        if contact_id is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact id not found')

        new_attr = body.get('new_attr')

        if new_attr is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Attributes not found')

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        if role.is_admin is True:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'This admin use any method')

        contact = contact_manager.update_contact(contact_id, role.id, new_attr)
        if contact is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact not found')

        result = contact.to_web_dict()

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    @bp.route(api_path(api_prefix, ApiMethods.updateAdminContact), methods=['POST'])
    @db_session
    def handle_update_admin_contact():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        contact_id = body.get('contact_id')

        if contact_id is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact id not found')

        new_attr = body.get('new_attr')

        if new_attr is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Attributes not found')

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        if role.is_admin is False:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Not admin')

        contact = contact_manager.update_admin_contact(contact_id, new_attr)
        if contact is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact not found')

        result = contact.to_web_dict()

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    @bp.route(api_path(api_prefix, ApiMethods.createContact), methods=['POST'])
    @db_session
    def handle_create_contact():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        name = body.get('name'),
        last_name = body.get('last_name'),
        patronymic = body.get('patronymic'),
        job_title = body.get('job_title'),
        email = body.get('email'),
        phone = body.get('phone')

        if phone is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Phone not found')

        phone = str(phone)
        if 10 <= len(phone) <= 12:
            if phone[0] == '8' and len(phone) == 11:
                phone = '+7' + phone[1:]
            elif phone[0] == '9' and len(phone) == 10:
                phone = '+7' + phone
            elif phone[0] == '7' and len(phone) == 11:
                phone = '+' + phone
            elif phone[0] == '+' and len(phone) == 12:
                phone = phone
            else:
                return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Invalid phone')

        if name is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Name not found')

        if last_name is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Last name not found')

        if job_title is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Job title id not found')

        if email is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Email not found')

        if patronymic is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Patronymic not found')

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        contact = contact_manager.create_contact(
            role.id,
            name[0],
            last_name[0],
            patronymic[0],
            job_title[0],
            email[0],
            phone
        )
        if contact is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Contact not created')

        result = contact.to_web_dict()

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    return bp
