import logging

from flask import Blueprint, request
from pony.orm import db_session

import config
from constants import ApiFrontEndResponseStatus
from api.api_manager import ApiManager
from api.utils.common import api_path
from db import database
from managers.role_manager import RoleManager

logger = logging.getLogger(__name__)


class ApiMethods:
    createAdmin = 'createAdmin'
    createUser = 'createUser'
    deleteAdmin = 'deleteAdmin'
    deleteUser = 'deleteUser'


role_manager = RoleManager(database)


def construct_blueprint(api_prefix):
    bp = Blueprint('role_api', __name__)

    @bp.route(api_path(api_prefix, ApiMethods.createAdmin), methods=['POST'])
    @db_session
    def handle_create_admin():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        default_token = config.APP_SETTINGS.get('default_token')
        if default_token is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        if token != default_token:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.create_role(True)
        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Role not created')

        result = role.to_web_dict()

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    @bp.route(api_path(api_prefix, ApiMethods.createUser), methods=['POST'])
    @db_session
    def handle_create_user():
        body = request.get_json()

        user_role = role_manager.create_role(False)

        if user_role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Role not created')

        result = user_role.to_web_dict()

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    @bp.route(api_path(api_prefix, ApiMethods.deleteAdmin), methods=['POST'])
    @db_session
    def handle_delete_admin():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None or role.is_admin is False:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Unable to delete role')

        result = role_manager.delete_role_by_token(token)

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    @bp.route(api_path(api_prefix, ApiMethods.deleteUser), methods=['POST'])
    @db_session
    def handle_delete_user():
        body = request.get_json()

        user_token = body.get('token')

        if user_token is None:
            return ApiManager.return_unauthorized()

        try:
            token = str(user_token)
        except:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Wrong token')

        role = role_manager.get_role_by_token(token)

        if role is None:
            return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.error, 'Unable to delete role')

        result = role_manager.delete_role_by_token(token)

        return ApiManager.get_front_end_response(ApiFrontEndResponseStatus.ok, result)

    return bp




