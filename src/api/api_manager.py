
from constants import ApiResponseStatus, ApiFrontEndResponseStatus, ForecastType, ProductsType
from flask import jsonify, Response


class ApiManager:
    @staticmethod
    def get_request_json(request):
        try:
            return request.get_json()
        except:
            return None

    @staticmethod
    def get_front_end_response(status, request_result, result=None, error_description=None):
        response = {
            'status': status
        }

        if status == ApiFrontEndResponseStatus.error:
            response['errorDescription'] = request_result
        elif status == ApiFrontEndResponseStatus.ok:
            response['result'] = request_result

        return jsonify(response)

    @staticmethod
    def return_unauthorized():
        return Response('Login required', 401)

