from db.database import database as db
from pony.orm import *


class Role(db.Entity):
    id = PrimaryKey(int, auto=True)
    is_admin = Required(bool)
    token = Required(str, unique=True)

    def to_web_dict(self):
        result = {
            'id': self.id,
            'is_admin': self.is_admin,
            'token': self.token,
        }

        return result
