from db.database import database as db
from pony.orm import *


class Contact(db.Entity):
    id = PrimaryKey(int, auto=True)
    creator_id = Required(int)
    name = Required(str, 255)
    last_name = Required(str, 255)
    patronymic = Optional(str)
    job_title = Required(str, 255)
    email = Required(str, 255)
    phone = Required(str, 255)
    is_deleted = Required(bool, default=False)

    def to_web_dict(self):
        result = {
            'id': self.id,
            'creator_id': self.creator_id,
            'name': self.name,
            'last_name': self.last_name,
            'patronymic': self.patronymic,
            'job_title': self.job_title,
            'email': self.email,
            'phone': self.phone,
            'is_deleted': self.is_deleted
        }

        return result