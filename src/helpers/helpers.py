import secrets
import string


class Helper:

    @staticmethod
    def generate_random_string(length):
        letters_and_digits = string.ascii_letters + string.digits
        crypt_rand_string = ''.join(secrets.choice(
            letters_and_digits) for i in range(length))
        return crypt_rand_string
