class ApiResponseStatus:
    ok = 'ok'
    dialog = 'dialog'
    error = 'error'


class ApiFrontEndResponseStatus:
    ok = 'ok'
    error = 'error'


class ForecastType:
    week = 'week'
    month = 'month'
    quarter = 'quarter'
    half_year = 'half_year'


class CoreProcessingStatus:
    ok = 'ok'
    error = 'error'


class ProductsType:
    finished_products = 'finished_products'
    components = 'components'
