import os


APP_SETTINGS = {
    'server_name': os.environ.get('SERVER_NAME', None),
    'default_token': os.environ.get('DEFAULT_TOKEN', 'OZjxcLpMraVC6iw4VGshVNHOxu6sS5JTTopSfCeoEYH5l1vou40tRuiQNYUbTuoL'),
    'db': {
        'host': os.environ.get('DB_HOST', 'localhost'),
        'port': int(os.environ.get('DB_PORT', 5432)),
        'name': os.environ.get('DB_NAME', 'test_app'),
        'user': os.environ.get('DB_USER', 'postgres'),
        'password': os.environ.get('DB_PASSWORD', 'postgres')
    },
    'api': {
        'version': '2',
        'host': os.environ.get('API_HOST', 'localhost'),
        'port': int(os.environ.get('API_PORT', 5004)),
        'prefix': os.environ.get('API_PREFIX', '/api')
    }
}

environment = str(os.environ.get('RUNTIME_ENVIRONMENT', 'dev')).lower()

if environment not in ['production', 'dev']:
    environment = 'dev'

RUNTIME_ENVIRONMENT = environment
