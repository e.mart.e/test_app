import config
from api import register_blueprints
from db import database
from flask import Flask


database.bind(provider='postgres',
              host=config.APP_SETTINGS['db']['host'],
              port=config.APP_SETTINGS['db']['port'],
              user=config.APP_SETTINGS['db']['user'],
              password=config.APP_SETTINGS['db']['password'],
              database=config.APP_SETTINGS['db']['name'])

database.generate_mapping(create_tables=True)

app = Flask(__name__)
register_blueprints(app, config.APP_SETTINGS)

app.run(host=config.APP_SETTINGS['api']['host'], port=config.APP_SETTINGS['api']['port'], threaded=True)
