FROM python:3.7.2

# mount host volumes

COPY ./src /srv

# install application dependencies

RUN cd /srv && pip install -r requirements.txt

WORKDIR /srv

CMD python run.py
